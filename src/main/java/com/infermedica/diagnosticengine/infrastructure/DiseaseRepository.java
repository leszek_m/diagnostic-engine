package com.infermedica.diagnosticengine.infrastructure;

import com.infermedica.diagnosticengine.domain.IdGenerator;
import com.infermedica.diagnosticengine.domain.model.Disease;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DiseaseRepository extends InMemorySimpleRepository<Disease> {
    @Autowired
    public DiseaseRepository(IdGenerator idGenerator) {
        super(idGenerator);
    }
}
