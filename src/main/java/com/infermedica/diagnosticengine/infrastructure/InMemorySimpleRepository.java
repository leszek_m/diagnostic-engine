package com.infermedica.diagnosticengine.infrastructure;

import com.infermedica.diagnosticengine.domain.IdGenerator;
import com.infermedica.diagnosticengine.domain.SimpleEntity;
import com.infermedica.diagnosticengine.domain.SimpleRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public abstract class InMemorySimpleRepository<T extends SimpleEntity> implements SimpleRepository<T> {
    private IdGenerator idGenerator;
    private final Set<T> entities = new ConcurrentHashMap<>().newKeySet();

    public InMemorySimpleRepository(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    @Override
    public T save(T entity) {
        if (entity.getId() == null || entity.getId().length() == 0) {
            entity.setId(idGenerator.generateId());
        }
        entities.add(entity);
        return entity;
    }

    @Override
    public Optional<T> findById(String id) {
        return entities.stream().filter(e -> id.equals(e.getId())).findAny();
    }

    @Override
    public Set<T> getAll() {
        return new HashSet<>(entities);     // instead of unmodifiable set
    }
}
