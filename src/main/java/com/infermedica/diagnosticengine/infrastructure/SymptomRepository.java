package com.infermedica.diagnosticengine.infrastructure;

import com.infermedica.diagnosticengine.domain.IdGenerator;
import com.infermedica.diagnosticengine.domain.model.Symptom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SymptomRepository extends InMemorySimpleRepository<Symptom> {
    @Autowired
    public SymptomRepository(IdGenerator idGenerator) {
        super(idGenerator);
    }
}
