package com.infermedica.diagnosticengine.domain.model;

import com.infermedica.diagnosticengine.domain.SimpleEntity;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Getter
public class Symptom implements SimpleEntity {
    private String id;
    @NotEmpty
    private String name;
    @NotNull
    private Sex sex;

    public Symptom(String name, Sex sex) {
        this.name = name;
        this.sex = sex;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Symptom symptom = (Symptom) o;
        return Objects.equals(id, symptom.id) &&
                name.equals(symptom.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
