package com.infermedica.diagnosticengine.domain.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Data
public class OccuringSymptom {
    @NotEmpty
    private String symptomId;
    @NotNull
    private Double probability;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OccuringSymptom that = (OccuringSymptom) o;
        return symptomId.equals(that.symptomId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symptomId);
    }
}
