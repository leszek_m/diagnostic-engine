package com.infermedica.diagnosticengine.domain.model;

import java.math.BigDecimal;

/**
 * Measure of how many people suffer from given disease in the entire population
 */
public enum Prevalence {
    // TODO maybe change back to double type
    RARE(new BigDecimal("0.0005")),
    MODERATE(new BigDecimal("0.005")),
    LIKELY(new BigDecimal("0.03"));

    private final BigDecimal value;

    private Prevalence(final BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }
}
