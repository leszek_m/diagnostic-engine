package com.infermedica.diagnosticengine.domain.model;

public enum Sex {
    MALE,
    FEMALE,
    BOTH
}
