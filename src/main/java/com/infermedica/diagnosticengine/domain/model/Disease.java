package com.infermedica.diagnosticengine.domain.model;

import com.infermedica.diagnosticengine.domain.SimpleEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

@Getter
@NoArgsConstructor
public class Disease implements SimpleEntity {
    private String id;
    @NotEmpty
    private String name;
    @NotNull
    private Prevalence prevalence;
    @NotNull
    private Sex sex;
    @NotEmpty @Valid
    private Set<OccuringSymptom> occuringSymptoms;

    public Disease(String name, Prevalence prevalence, Sex sex, Set<OccuringSymptom> occuringSymptoms) {
        this.name = name;
        this.prevalence = prevalence;
        this.sex = sex;
        this.occuringSymptoms = occuringSymptoms;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disease disease = (Disease) o;
        return Objects.equals(id, disease.id) &&
                name.equals(disease.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
