package com.infermedica.diagnosticengine.domain;

import java.util.Optional;
import java.util.Set;

public interface SimpleRepository<T extends SimpleEntity> {
    T save(T entity);
    Optional<T> findById(String id);
    Set<T> getAll();
}
