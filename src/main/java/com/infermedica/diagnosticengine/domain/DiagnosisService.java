package com.infermedica.diagnosticengine.domain;

import com.infermedica.diagnosticengine.domain.dto.MostProbableDisease;
import com.infermedica.diagnosticengine.domain.exceptions.InvalidDiagnosisRequestException;
import com.infermedica.diagnosticengine.domain.model.Disease;
import com.infermedica.diagnosticengine.domain.model.OccuringSymptom;
import com.infermedica.diagnosticengine.domain.model.Sex;
import com.infermedica.diagnosticengine.domain.model.Symptom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DiagnosisService {
    private SimpleRepository<Disease> diseaseRepository;
    private SimpleRepository<Symptom> symptomRepository;

    @Autowired
    public DiagnosisService(SimpleRepository<Disease> diseaseRepository, SimpleRepository<Symptom> symptomRepository) {
        this.diseaseRepository = diseaseRepository;
        this.symptomRepository = symptomRepository;
    }

    public MostProbableDisease diagnoseDisease(Sex sex, List<String> reportedSymptomsIds) {
        validateParameters(sex, reportedSymptomsIds);

        Map<Disease, Set<OccuringSymptom>> diseasesRelevantSymptoms = diseaseRepository.getAll().parallelStream()
                .filter(disease -> disease.getSex() == sex || disease.getSex() == Sex.BOTH)     // filter only sex relevant diseases
                .collect(Collectors.toMap(disease -> disease, disease -> getRelevantSymptoms(disease, reportedSymptomsIds)));
        Map<Disease, Double> diseasesLikelihood = diseasesRelevantSymptoms.entrySet().parallelStream()
                .filter(diseaseSymptomsEntry -> ! diseaseSymptomsEntry.getValue().isEmpty())    // filter out diseases without relevant symptoms
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        e -> e.getValue().stream().mapToDouble(OccuringSymptom::getProbability).sum()
                                * e.getKey().getPrevalence().getValue().doubleValue()));
        final Disease mostProbaleDisease = diseasesLikelihood.entrySet().parallelStream()
                .max(Map.Entry.comparingByValue()).get().getKey();

        return new MostProbableDisease(mostProbaleDisease.getId(), mostProbaleDisease.getName());
    }

    private void validateParameters(Sex sex, List<String> reportedSymptomsIds) {
        if (sex == Sex.BOTH) {
            throw new InvalidDiagnosisRequestException("There must be actual 'sex' set - BOTH value is not permitted");
        }
        reportedSymptomsIds.stream().forEach(this::verifySymptomExistence);
    }

    private void verifySymptomExistence(String symptomId) {
        Optional<Symptom> foundSymptom = symptomRepository.findById(symptomId);
        if (! foundSymptom.isPresent()) {
            throw new InvalidDiagnosisRequestException("Reported symptom <" + symptomId + "> doesn't exist");
        }
    }

    private Set<OccuringSymptom> getRelevantSymptoms(Disease disease, List<String> patientReportedSymptoms) {
        return disease.getOccuringSymptoms().stream()
                .filter(symptom -> patientReportedSymptoms.contains(symptom.getSymptomId()))
                .collect(Collectors.toSet());
    }
}
