package com.infermedica.diagnosticengine.domain.exceptions;

/**
 * Means that in diagnosis request (input data) some logical incorrectness exist
 */
public class InvalidDiagnosisRequestException extends RuntimeException {
    public InvalidDiagnosisRequestException(String message) {
        super(message);
    }
}
