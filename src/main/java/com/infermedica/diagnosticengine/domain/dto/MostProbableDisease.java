package com.infermedica.diagnosticengine.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MostProbableDisease {
    private String id;
    private String name;
}
