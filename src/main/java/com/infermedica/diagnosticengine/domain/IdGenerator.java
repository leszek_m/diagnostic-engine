package com.infermedica.diagnosticengine.domain;

public interface IdGenerator {
    String generateId();
}
