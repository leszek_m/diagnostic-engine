package com.infermedica.diagnosticengine.domain;

public interface SimpleEntity {
    String getId();
    void setId(String id);
}
