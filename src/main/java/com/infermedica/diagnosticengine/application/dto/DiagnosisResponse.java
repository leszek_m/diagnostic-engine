package com.infermedica.diagnosticengine.application.dto;

import com.infermedica.diagnosticengine.domain.dto.MostProbableDisease;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class DiagnosisResponse {
    MostProbableDisease mostProbableDisease;
}
