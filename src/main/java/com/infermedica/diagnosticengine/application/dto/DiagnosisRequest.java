package com.infermedica.diagnosticengine.application.dto;

import com.infermedica.diagnosticengine.domain.model.Sex;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@AllArgsConstructor
public class DiagnosisRequest {
    @NotNull
    private Sex sex;
    @NotEmpty
    private List<String> reportedSymptoms;
}
