package com.infermedica.diagnosticengine.application.exceptions.handling;

import com.infermedica.diagnosticengine.domain.exceptions.InvalidDiagnosisRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class RestControllerlExceptionHandler {
    @ExceptionHandler(InvalidDiagnosisRequestException.class)
    public ResponseEntity<ErrorMessage> handleInvalidDiagnosisRequestException(InvalidDiagnosisRequestException e) {
        ErrorMessage errorMsg = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), LocalDateTime.now(), e.getMessage());
        return new ResponseEntity<>(errorMsg, HttpStatus.BAD_REQUEST);
    }
}
