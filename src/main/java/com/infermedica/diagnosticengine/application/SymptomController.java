package com.infermedica.diagnosticengine.application;

import com.infermedica.diagnosticengine.domain.SimpleRepository;
import com.infermedica.diagnosticengine.domain.model.Symptom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping(value = "/symptoms", produces = MediaType.APPLICATION_JSON_VALUE)
public class SymptomController {
    private SimpleRepository<Symptom> symptomRepository;

    @Autowired
    public SymptomController(SimpleRepository<Symptom> symptomRepository) {
        this.symptomRepository = symptomRepository;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Symptom> createSymptom(@Valid @RequestBody Symptom symptom) {
        Symptom savedSymptom = symptomRepository.save(symptom);
        return new ResponseEntity<>(savedSymptom, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Set<Symptom>> getAllSymptoms() {
        return new ResponseEntity<>(symptomRepository.getAll(), HttpStatus.OK);
    }
}
