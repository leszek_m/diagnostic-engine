package com.infermedica.diagnosticengine.application;

import com.infermedica.diagnosticengine.domain.SimpleRepository;
import com.infermedica.diagnosticengine.domain.model.Disease;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping(value = "/diseases", produces = MediaType.APPLICATION_JSON_VALUE)
public class DiseaseController {
    private SimpleRepository<Disease> diseaseRepository;
    
    @Autowired
    public DiseaseController(SimpleRepository<Disease> diseaseRepository) {
        this.diseaseRepository = diseaseRepository;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Disease> createDisease(@Valid @RequestBody Disease disease) {
        Disease savedDisease = diseaseRepository.save(disease);
        return new ResponseEntity<>(savedDisease, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Set<Disease>> getAllDiseases() {
        return new ResponseEntity<>(diseaseRepository.getAll(), HttpStatus.OK);
    }
}
