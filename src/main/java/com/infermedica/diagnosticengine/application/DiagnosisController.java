package com.infermedica.diagnosticengine.application;

import com.infermedica.diagnosticengine.application.dto.DiagnosisRequest;
import com.infermedica.diagnosticengine.application.dto.DiagnosisResponse;
import com.infermedica.diagnosticengine.domain.DiagnosisService;
import com.infermedica.diagnosticengine.domain.dto.MostProbableDisease;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/diagnosis")
public class DiagnosisController {
    private DiagnosisService diagnosisService;

    @Autowired
    public DiagnosisController(DiagnosisService diagnosisService) {
        this.diagnosisService = diagnosisService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DiagnosisResponse> diagnoseDisease(@Valid @RequestBody DiagnosisRequest request) {
        MostProbableDisease diagnosedDisease = diagnosisService.diagnoseDisease(request.getSex(),
                request.getReportedSymptoms());
        return new ResponseEntity<>(new DiagnosisResponse(diagnosedDisease), HttpStatus.OK);
    }
}
