correct_request = { "sex": "FEMALE", "reportedSymptoms": [ "5c1f1278-cd74-4d13-a926-ee6bdd3ea82d", "25d03aa2-a9ec-4c81-87dd-7ec52f1168ce", "0faa2223-6281-4885-8f73-1566219d2c01" ] }
request_with_no_sex_value = { "sex": "", "reportedSymptoms": [ "5c1f1278-cd74-4d13-a926-ee6bdd3ea82d", "25d03aa2-a9ec-4c81-87dd-7ec52f1168ce", "0faa2223-6281-4885-8f73-1566219d2c01" ] }
request_with_wrong_sex_value = { "sex": "MAN", "reportedSymptoms": [ "5c1f1278-cd74-4d13-a926-ee6bdd3ea82d", "25d03aa2-a9ec-4c81-87dd-7ec52f1168ce", "0faa2223-6281-4885-8f73-1566219d2c01" ] }
request_with_no_reported_symptoms = { "sex": "MALE", "reportedSymptoms": [ ] }
request_with_ambiguous_sex_field = { "sex": "BOTH", "reportedSymptoms": [ "5c1f1278-cd74-4d13-a926-ee6bdd3ea82d", "25d03aa2-a9ec-4c81-87dd-7ec52f1168ce", "0faa2223-6281-4885-8f73-1566219d2c01" ] }
