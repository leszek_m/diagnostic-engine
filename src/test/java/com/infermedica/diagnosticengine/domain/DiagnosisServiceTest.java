package com.infermedica.diagnosticengine.domain;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.infermedica.diagnosticengine.domain.dto.MostProbableDisease;
import com.infermedica.diagnosticengine.domain.exceptions.InvalidDiagnosisRequestException;
import com.infermedica.diagnosticengine.domain.model.Disease;
import com.infermedica.diagnosticengine.domain.model.Sex;
import com.infermedica.diagnosticengine.domain.model.Symptom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DiagnosisServiceTest {
    private static final String DISEASES_FILE_PATH = "src/test/resources/sample_data/diseases.json";

    private SimpleRepository<Disease> diseaseRepositoryMock;
    private SimpleRepository<Symptom> symptomRepositoryMock;

    private DiagnosisService diagnosisService;

    @BeforeEach
    public void initialize() {
        diseaseRepositoryMock = (SimpleRepository<Disease>) mock(SimpleRepository.class);
        symptomRepositoryMock = (SimpleRepository<Symptom>) mock(SimpleRepository.class);
        diagnosisService = new DiagnosisService(diseaseRepositoryMock, symptomRepositoryMock);
    }

    @Test
    public void shouldThrowInvalidDiagnosisRequestExceptionForAmbiguousSexValue() {
        InvalidDiagnosisRequestException thrownException = assertThrows(
                InvalidDiagnosisRequestException.class,
                () -> diagnosisService.diagnoseDisease(Sex.BOTH, List.of("8c3303f1-b066-4dc6-a103-4b5c1f322887")),
                "Expected throwing InvalidDiagnosisRequestException for wrong sex value"
        );
        assertEquals("There must be actual 'sex' set - BOTH value is not permitted", thrownException.getMessage());
    }

    @Test
    public void shouldThrowInvalidDiagnosisRequestExceptionForNonexistentReportedSymptom() {
        // given
        final String reportedSymptomId = "8c3303f1-b066-4dc6-a103-4b5c1f322887";
        when(symptomRepositoryMock.findById(reportedSymptomId)).thenReturn(Optional.empty());
        // when and then
        InvalidDiagnosisRequestException thrownException = assertThrows(
                InvalidDiagnosisRequestException.class,
                () -> diagnosisService.diagnoseDisease(Sex.FEMALE, List.of(reportedSymptomId)),
                "Expected throwing InvalidDiagnosisRequestException for wrong sex value"
        );
        assertEquals("Reported symptom <" + reportedSymptomId + "> doesn't exist", thrownException.getMessage());
    }

    @Test
    public void shouldCalculateMostProbableDisease() throws IOException {
        // given
        when(symptomRepositoryMock.findById(anyString())).thenReturn(Optional.of(mock(Symptom.class)));
        final Set<Disease> allDiseases = loadDiseases();
        when(diseaseRepositoryMock.getAll()).thenReturn(allDiseases);
        List<String> reportedSymptomsIds = Arrays.asList("0faa2223-6281-4885-8f73-1566219d2c01",
                "5c1f1278-cd74-4d13-a926-ee6bdd3ea82d", "25d03aa2-a9ec-4c81-87dd-7ec52f1168ce");
        // when
        MostProbableDisease diagnosedDisease = diagnosisService.diagnoseDisease(Sex.MALE, reportedSymptomsIds);
        // then
        assertEquals("67fd6612-eed1-4a97-983f-216b674865de", diagnosedDisease.getId(), "Wrong diagnose");
        assertEquals("flu", diagnosedDisease.getName(), "Wrong diagnose");
    }

    private Set<Disease> loadDiseases() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(new File(DISEASES_FILE_PATH), new TypeReference<Set<Disease>>(){});
    }

    // TODO - write test for diseases specific for MALE or FEMALE only
}