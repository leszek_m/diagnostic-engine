package com.infermedica.diagnosticengine.application;

import com.infermedica.diagnosticengine.domain.model.Sex;
import com.infermedica.diagnosticengine.domain.model.Symptom;
import com.infermedica.diagnosticengine.infrastructure.SymptomRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.ResourceUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = SymptomController.class)
class SymptomControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private SymptomRepository symptomRepositoryMock;

    private static Properties requests;

    @BeforeAll
    public static void initializeBeforeAll() throws IOException {
        InputStream in = new FileInputStream(ResourceUtils.getFile("classpath:symptoms_requests.txt"));
        requests = new Properties();
        requests.load(in);
    }

    @Test
    void shouldCreateSymptomFromCorrectJsonAndReturnWithGeneratedId() throws Exception {
        // given
        final String symptomId = "1685c9f1-aa6c-4687-8ff6-19da4f671a52";
        doAnswer(invocation -> {
            Symptom symptom = invocation.getArgument(0);
            symptom.setId(symptomId);
            return symptom;
        }).when(symptomRepositoryMock).save(any(Symptom.class));
        // when
        final String response = mockMvc.perform(post("/symptoms").content(requests.getProperty("correct_request"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn().getResponse().getContentAsString();
        final JSONObject responseJson = new JSONObject(response);
        // then
        assertDoesNotThrow(() -> responseJson.get("id"), "'Id' field not found in response");
        assertEquals(symptomId, responseJson.getString("id"), "Wrong id received");
    }

    @Test
    void shouldReturnBadRequestCodeForSymptomWithEmptyName() throws Exception {
        mockMvc.perform(post("/symptoms").content(requests.getProperty("request_with_empty_name_field"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    void shouldReturnBadRequestCodeForSymptomWithoutSex() throws Exception {
        mockMvc.perform(post("/symptoms").content(requests.getProperty("request_without_sex_field"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    void shouldGetAllSymptoms() throws Exception {
        // given
        final Set<Symptom> symptoms = Set.of(
                new Symptom("headache", Sex.FEMALE),
                new Symptom("simple fracture", Sex.BOTH)
        );
        when(symptomRepositoryMock.getAll()).thenReturn(symptoms);
        // when
        final String response = mockMvc.perform(get("/symptoms").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
        JSONArray responseSymptoms = new JSONArray(response);
        // then
        assertEquals(2, responseSymptoms.length(), "Wrong number of symptoms received");
    }
}