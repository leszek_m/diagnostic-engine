package com.infermedica.diagnosticengine.application;

import com.infermedica.diagnosticengine.domain.DiagnosisService;
import com.infermedica.diagnosticengine.domain.dto.MostProbableDisease;
import com.infermedica.diagnosticengine.domain.exceptions.InvalidDiagnosisRequestException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.ResourceUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = DiagnosisController.class)
class DiagnosisControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private DiagnosisService diagnosisServiceMock;

    private static Properties requests;

    @BeforeAll
    public static void initializeBeforeAll() throws IOException {
        InputStream in = new FileInputStream(ResourceUtils.getFile("classpath:diagnosis_requests.txt"));
        requests = new Properties();
        requests.load(in);
    }

    @Test
    public void shouldReceiveMostProbableDiseaseForGivenCorrectDiagnosisRequest() throws Exception {
        // given
        MostProbableDisease diagnosedDisease = new MostProbableDisease("ac2820ca-8235-4ce7-91d0-c9f1e650ae64", "covid-19");
        when(diagnosisServiceMock.diagnoseDisease(any(), any())).thenReturn(diagnosedDisease);
        // when
        final String response = mockMvc.perform(post("/diagnosis").content(requests.getProperty("correct_request"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        final JSONObject responseJson = new JSONObject(response);
        // then
        assertEquals(diagnosedDisease.getName(), responseJson.getJSONObject("mostProbableDisease").getString("name"),
                "Wrong diagnosed disease name");
    }

    @Test
    public void shouldReturnBadRequestCodeForEmptySexValue() throws Exception {
        mockMvc.perform(post("/diagnosis").content(requests.getProperty("request_with_no_sex_value"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void shouldReturnBadRequestCodeForRequestWithWrongSexValue() throws Exception {
        mockMvc.perform(post("/diagnosis").content(requests.getProperty("request_with_wrong_sex_value"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void shouldReturnBadRequestCodeForRequestWithNoReportedSymptoms() throws Exception {
        mockMvc.perform(post("/diagnosis").content(requests.getProperty("request_with_no_reported_symptoms"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void shouldReturnBadRequestCodeForRequestThrowingInvalidDiagnosisRequestException() throws Exception {
        // given
        when(diagnosisServiceMock.diagnoseDisease(any(), any())).thenThrow(InvalidDiagnosisRequestException.class);
        // when and then
        mockMvc.perform(post("/diagnosis").content(requests.getProperty("request_with_ambiguous_sex_field"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
    }
}