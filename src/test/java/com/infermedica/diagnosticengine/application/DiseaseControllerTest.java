package com.infermedica.diagnosticengine.application;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.infermedica.diagnosticengine.domain.model.Disease;
import com.infermedica.diagnosticengine.domain.model.Symptom;
import com.infermedica.diagnosticengine.infrastructure.DiseaseRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = DiseaseController.class)
class DiseaseControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private DiseaseRepository diseaseRepositoryMock;

    private static Properties requests;

    @BeforeAll
    public static void initializeBeforeAll() throws IOException {
        InputStream in = new FileInputStream(ResourceUtils.getFile("classpath:diseases_requests.txt"));
        requests = new Properties();
        requests.load(in);
    }

    @Test
    void shouldCreateDiseaseFromCorrectJsonAndReturnWithGeneratedId() throws Exception {
        // given
        final String diseaseId = "67fd6612-eed1-4a97-983f-216b674865de";
        doAnswer(invocation -> {
            Disease disease = invocation.getArgument(0);
            disease.setId(diseaseId);
            return disease;
        }).when(diseaseRepositoryMock).save(any(Disease.class));
        // when
        final String response = mockMvc.perform(post("/diseases").content(requests.getProperty("correct_request"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn().getResponse().getContentAsString();
        final JSONObject responseJson = new JSONObject(response);
        // then
        assertDoesNotThrow(() -> responseJson.get("id"), "'Id' field not found in response");
        assertEquals(diseaseId, responseJson.getString("id"), "Wrong id received");
    }

    @Test
    void shouldReturnBadRequestCodeForDiseaseWithEmptySymptoms() throws Exception {
        mockMvc.perform(post("/diseases").content(requests.getProperty("request_with_empty_symptoms"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    void shouldReturnBadRequestCodeForSymptomWithUnknownPrevalence() throws Exception {
        mockMvc.perform(post("/diseases").content(requests.getProperty("request_with_unknown_prevalence"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    void shouldGetAllDiseases() throws Exception {
        // given
        ObjectMapper objectMapper = new ObjectMapper();
        final Set<Disease> diseases = objectMapper.readValue(
                new File("src/test/resources/sample_data/diseases.json"), new TypeReference<Set<Disease>>() {});
        when(diseaseRepositoryMock.getAll()).thenReturn(diseases);
        // when
        final String response = mockMvc.perform(get("/diseases").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
        JSONArray responseDiseases = new JSONArray(response);
        // then
        assertEquals(diseases.size(), responseDiseases.length(), "Wrong number of diseases");
    }
}