## This is a simple diagnosis application that enables users/patients to initially check their symptoms for the most probable disease

Warning: Application is just a tool, very helpful yet but only doctor can diagnose you rightly.

To compile and run this application you need to have following programs installed on your computer:
- Java 11 (tested on 11.0.8)
- Gradle 6 or above (tested on 6.6.1)

In the project directory please execute  
`.scripts/compile.sh` to get runnable application; then  
`.scripts/run.sh` to boot it

Application will start listening on port 8080 on your computer. Make sure this port is free. 

************************************************************************************************************************************

### Additional questions:
1. Which technologies did you choose to implement the application? Why did you choose them?  
I chose Spring Boot to create a stand-alone application. It is quite lightweight technology, complex but easy to configure and
of course mature.
2. What was the most difficult part of the task?  
The most difficult for me was to choose method of creation entities' identifiers. I do not mean data type or algorithm. I was
thinking some time where it should be created and mostly how not to harm good principles of architecture and coding.
3. Assess time complexity of diagnosis algorithm.  
I hate this question but let's say it is not bigger than _O(D+D*RS+D*RS+D)_ where _D_ is for diseases number and _RS_ is
for reported symptoms. 
4. Do you see any problems with the current diagnosis algorithm?  
Yes. For instance the Prevalence should be a linear value or at least there should be more values to choose from. It has
big impact on result.
5. How would you improve the quality of diagnosis?  
Well, apart from above suggestion I would add two things:
    * In diagnosis of some disease it would be good to introduce scale that is based on a few binary values. When a specific number
    of them is true, it means that verified disease is highly probable. For example in throat bacterial infections doctors use
    Centor/McIsaac scale to decide what to do with patient according to number of points from 0 to 4 that were found true.
    * Besides symptoms reported by patient, it would be useful to let him/her choose how severe it is. If someone has a runny nose
    that is specific to cold, so its probability is high but symptom is very light I think it makes the result a bit false.
    But I am not a doctor :) 


